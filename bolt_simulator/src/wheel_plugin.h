#ifndef WHEEL_PLUGIN_H
#define WHEEL_PLUGIN_H

#include <boost/bind.hpp>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/common/common.hh>

#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/advertise_options.h>
#include <std_msgs/Float64.h>

#include <ignition/math/Vector3.hh>

#include <iostream>

namespace gazebo
{

class WheelPlugin : public ModelPlugin
{
private:
    physics::ModelPtr model;
    event::ConnectionPtr updateConnection;
    
    ros::NodeHandle *nh;
    ros::Publisher ang_vel_pub_;
    ros::Subscriber torque_sub_;
    
    sdf::ElementPtr sdf_;
    
    physics::ModelPtr model_;
    physics::WorldPtr world_;
    physics::LinkPtr wheel_link_;
    
    std::string wheelLinkName_;
    std::string topic_name_;
    std::string robot_namespace_;

    int update_rate_;
    
    double update_interval_;
    double torque_input_ = 0;
    double max_torque_;

    ignition::math::Vector3d max_velocity_;
    ignition::math::Vector3d wheel_ang_vel_;

    common::Time last_update_time_;
    
    void TorqueCallback(const std_msgs::Float64::ConstPtr& msg);

public:
    WheelPlugin();
    ~WheelPlugin();
    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);
    virtual void UpdateChild();
};

}

#endif // WHEEL_PLUGIN_H
