/* 
 * Author: Vitor Alves Pavani
 * Date: 22/09/2020
 * Desc: Example plugin code to control a wheel in gazebo.
 */

#include "wheel_plugin.h"

namespace gazebo
{

WheelPlugin::WheelPlugin() { }

WheelPlugin::~WheelPlugin() { }

void WheelPlugin::TorqueCallback(const std_msgs::Float64::ConstPtr& msg)
{
    this->torque_input_ = msg->data;
}

void WheelPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
    this->sdf_ = _sdf;
    this->model = _parent;
    int argc = 0;
    this->robot_namespace_ = "";
    char **argv = NULL;
    ros::init(argc, argv, "wheel_plugin node",
            ros::init_options::NoSigintHandler |
            ros::init_options::AnonymousName);

    if(!ros::isInitialized())
    {
        ROS_FATAL_STREAM("ROS Not initialized");
        return;
    }

    // load parameters from sdf
    if (this->sdf_->HasElement("wheelLink"))
    {
        this->wheelLinkName_ = this->sdf_->Get<std::string>("wheelLink");
    }
    else
    {
        ROS_FATAL_STREAM_NAMED("wheelPlugin", "<wheelLink> parameter not set in SDF.");
        return;
    }
    this->wheel_link_ = _parent->GetLink(this->wheelLinkName_);

    if (!this->wheel_link_)
    {
        ROS_FATAL_NAMED("wheel_plugin", " error: wheelLink: %s does not exist.",  this->wheelLinkName_.c_str());
        return;
    }
    if (this->sdf_->HasElement("updateRate"))
    {
        this->update_rate_ = this->sdf_->Get<int>("updateRate");
    }
    else
    {
        ROS_INFO_NAMED("wheelPlugin", "<updateRate> parameter not set in SDF. Default to 30hz.");
        this->update_rate_ = 30;
        this->update_interval_ = 1.0 / this->update_rate_;
    }
    if (this->sdf_->HasElement("topicPrefix"))
    {
        this->topic_name_ = this->sdf_->Get<int>("topicPrefix");
    }
    else
    {
        this->topic_name_ = this->robot_namespace_ + "/" + this->wheelLinkName_;
        ROS_INFO_NAMED("wheelPlugin", "<topicPrefix> parameter not set in SDF. Default to %s.", this->topic_name_.c_str());        
    }
    // prevent force being added if current angular velocity > maxVelocity. 
    if (this->sdf_->HasElement("maxVelocity"))
    {
        this->max_velocity_.Z(this->sdf_->Get<double>("maxVelocity"));
    }
    else
    {
        ROS_INFO_NAMED("wheelPlugin", "<maxVelocity> parameter not set in SDF. Default to 1 rad/s.");
        this->max_velocity_.Z(1);      
    }
    if (this->sdf_->HasElement("maxTorque"))
    {
        this->max_torque_ = this->sdf_->Get<double>("maxTorque");
    }
    else
    {
        ROS_INFO_NAMED("wheelPlugin", "<maxTorque> parameter not set in SDF. Default to 200N.");
        this->max_torque_ = 200;
    }

    nh = new ros::NodeHandle("wheel_plugin");

    ang_vel_pub_ = nh->advertise<std_msgs::Float64>(topic_name_ + "/angular_vel", 10);
    torque_sub_ = nh->subscribe<std_msgs::Float64>(topic_name_ + "/torque_input", 1, &WheelPlugin::TorqueCallback, this);
    last_update_time_ = model->GetWorld()->SimTime();

    updateConnection = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&WheelPlugin::UpdateChild, this));
    
    ROS_INFO_STREAM("wheel_plugin initialized.");
    
}

void WheelPlugin::UpdateChild()
{
    common::Time current_time = model->GetWorld()->SimTime();
    double seconds_since_last_update = ( current_time - last_update_time_ ).Double();

    if(fabs(torque_input_) > this->max_torque_)
    {
    	torque_input_ = 250;
    }
    if(seconds_since_last_update >= update_interval_)
    { 
        last_update_time_ += common::Time( update_interval_ );
        wheel_ang_vel_ = wheel_link_->RelativeAngularVel();
        std_msgs::Float64 msg;
        msg.data = wheel_ang_vel_.Z();
        ang_vel_pub_.publish(msg);
        wheel_link_->AddRelativeTorque(ignition::math::Vector3d(0,0, torque_input_));
	torque_input_ = 0;
    }
    // limit wheel angular velocity
    if (wheel_ang_vel_.Z() > max_velocity_.Z())
    {
        wheel_link_->SetAngularVel(max_velocity_);
    }
    if (wheel_ang_vel_.Z() < -max_velocity_.Z())
    {
        wheel_link_->SetAngularVel(-max_velocity_);
    }
}

GZ_REGISTER_MODEL_PLUGIN(WheelPlugin)
}
